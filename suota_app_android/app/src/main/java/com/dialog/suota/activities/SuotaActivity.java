/*
 *******************************************************************************
 *
 * Copyright (C) 2016 Dialog Semiconductor, unpublished work. This computer
 * program includes Confidential, Proprietary Information and is a Trade
 * Secret of Dialog Semiconductor. All use, disclosure, and/or reproduction
 * is prohibited unless authorized in writing. All Rights Reserved.
 *
 * bluetooth.support@diasemi.com
 *
 *******************************************************************************
 */

package com.dialog.suota.activities;


import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.dialog.suota.R;

public abstract class SuotaActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			getWindow().setStatusBarColor(getResources().getColor(R.color.status_bar_background));
			getWindow().setNavigationBarColor(getResources().getColor(R.color.navigation_bar_background));
		}
		ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#347ab8"));
		ActionBar actionBar = getSupportActionBar();
		if (actionBar != null) {
			actionBar.setBackgroundDrawable(colorDrawable);
			actionBar.setTitle("SUOTA");
			actionBar.setSubtitle("Dialog Semiconductor");
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		menu.findItem(R.id.restart_scan).setVisible(false);
		menu.findItem(R.id.disconnect).setVisible(false);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		switch (id) {
			case R.id.restart_scan:
			case R.id.disconnect:
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void changeActionbarTitle(String subtitle) {
		ActionBar actionBar = getSupportActionBar();
		if (actionBar != null) {
			actionBar.setSubtitle(subtitle);
		}
		Toolbar toolbar = (Toolbar) findViewById(R.id.sensor_toolbar);
		if (toolbar != null) {
			toolbar.setTitle("SUOTA");
			toolbar.setSubtitle(subtitle);
		}
	}

}
