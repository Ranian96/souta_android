/*
 *******************************************************************************
 *
 * Copyright (C) 2016 Dialog Semiconductor, unpublished work. This computer
 * program includes Confidential, Proprietary Information and is a Trade
 * Secret of Dialog Semiconductor. All use, disclosure, and/or reproduction
 * is prohibited unless authorized in writing. All Rights Reserved.
 *
 * bluetooth.support@diasemi.com
 *
 *******************************************************************************
 */

package com.dialog.suota.global;

/**
 * ScanItem object
 */
public class ScanItem {
    String scanName;
    String scanDescription;
    int scanSignal;
    boolean scanPaired;


    public ScanItem(String scanName, String scanDescription, int scanSignal, boolean scanPaired) {
        this.scanName = scanName;
        this.scanDescription = scanDescription;
        this.scanSignal = scanSignal;
        this.scanPaired = scanPaired;
    }
}