/*
 *******************************************************************************
 *
 * Copyright (C) 2016 Dialog Semiconductor, unpublished work. This computer
 * program includes Confidential, Proprietary Information and is a Trade
 * Secret of Dialog Semiconductor. All use, disclosure, and/or reproduction
 * is prohibited unless authorized in writing. All Rights Reserved.
 *
 * bluetooth.support@diasemi.com
 *
 *******************************************************************************
 */

package com.dialog.suota.global;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.util.AttributeSet;
import android.view.View;

import com.dialog.suota.R;


/**
 * Drawable for signal bars
 */
public class SignalBar extends View {
    private Paint mPaint;
    String TAG = "SignalBar";
    public boolean first = true, second = false, third = false, last = false, unavailable = false, paired = false;

    public SignalBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        ShapeDrawable mDrawable = new ShapeDrawable(new RectShape());
        mPaint = new Paint();
        mPaint.setColor(Color.GREEN);
        mPaint.setStrokeWidth(8);

        setWillNotDraw(false);
        invalidate();
    }

    public void setBool(boolean one, boolean two, boolean three, boolean four, boolean unava, boolean paired) {
        first = one;
        second = two;
        third = three;
        last = four;
        unavailable = unava;
        this.paired = paired;
        invalidate();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int x = canvas.getMaximumBitmapWidth(), y = canvas.getMaximumBitmapHeight(), width = getWidth() / 5, height = getHeight() / 5;
        int color = getResources().getColor(R.color.signal_bar_colour), nonactive = getResources().getColor(R.color.signal_bar_non_active_colour);
        if (!unavailable) {
            //First bar
            mPaint.setStyle(Paint.Style.FILL);
            mPaint.setColor(first ? color : nonactive);
            canvas.drawRect(width, height * 4, x, y, mPaint);
            mPaint.setStyle(Paint.Style.STROKE);
            mPaint.setColor(Color.WHITE);
            canvas.drawRect(width, height * 4, x, y, mPaint);

            //Second bar
            mPaint.setStyle(Paint.Style.FILL);
            mPaint.setColor(second ? color : nonactive);
            canvas.drawRect(width * 2, height * 3, x, y, mPaint);
            mPaint.setStyle(Paint.Style.STROKE);
            mPaint.setColor(Color.WHITE);
            canvas.drawRect(width * 2, height * 3, x, y, mPaint);

            //Third bar
            mPaint.setStyle(Paint.Style.FILL);
            mPaint.setColor(third ? color : nonactive);
            canvas.drawRect(width * 3, height * 2, x, y, mPaint);
            mPaint.setStyle(Paint.Style.STROKE);
            mPaint.setColor(Color.WHITE);
            canvas.drawRect(width * 3, height * 2, x, y, mPaint);

            //Last bar
            mPaint.setStyle(Paint.Style.FILL);
            mPaint.setColor(last ? color : nonactive);
            canvas.drawRect(width * 4, height, x, y, mPaint);
            mPaint.setStyle(Paint.Style.STROKE);
            mPaint.setColor(Color.WHITE);
            canvas.drawRect(width * 4, height, width * 5 - 1, y, mPaint);
        }
    }

}