/*
 *******************************************************************************
 *
 * Copyright (C) 2016 Dialog Semiconductor, unpublished work. This computer
 * program includes Confidential, Proprietary Information and is a Trade
 * Secret of Dialog Semiconductor. All use, disclosure, and/or reproduction
 * is prohibited unless authorized in writing. All Rights Reserved.
 *
 * bluetooth.support@diasemi.com
 *
 *******************************************************************************
 */

package com.dialog.suota.activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.dialog.suota.R;
import com.dialog.suota.data.File;
import com.dialog.suota.data.Statics;
import com.dialog.suota.data.Uuid;
import com.dialog.suota.global.ScanAdapter;
import com.dialog.suota.global.ScanItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.UUID;


public class ScanActivity extends SuotaActivity implements OnItemClickListener {
    private final static String TAG = "ScanActivity";
    private final static int REQUEST_ENABLE_BT = 1;
    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;
    private static final int PERMISSION_REQUEST_EXTERNAL_STORAGE = 112;

    private static final int PERMISSION_REQUEST = 0;
    private boolean permissionsOK = Build.VERSION.SDK_INT < 23;
    private boolean locationPermission = Build.VERSION.SDK_INT < 23;
    private boolean storagePermission = Build.VERSION.SDK_INT < 23;
    private boolean permissionRequestPending;
    private boolean displayedLocationServicesInfo;

    private boolean isScanning = false;
    private boolean showBondedDevices;

    private BluetoothAdapter mBluetoothAdapter;
    private ScannerApi scannerApi;
    private HashMap<String, BluetoothDevice> scannedDevices;

    private ArrayList<BluetoothDevice> bluetoothDeviceList;
    ScanAdapter bluetoothScanAdapter;
    ArrayList<ScanItem> deviceNameList;

    private ListView deviceListView;
    private MenuItem menuItemRefresh;

    private Handler handler;
    private Runnable scanTimer;

    private static class FoundDevice {
        String name;
        String address;
        boolean paired;

        public FoundDevice(String name, String address, boolean paired) {
            this.name = name;
            this.address = address;
            this.paired = paired;
        }
    }

    private static class ListAdapterViews {
        TextView name;
        TextView address;
    }

    private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(final BluetoothDevice device, final int rssi,
                             final byte[] scanRecord) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    List<UUID> uuids = Uuid.parseFromAdvertisementData(scanRecord);
                    for (UUID uuid : uuids) {
                        if (uuid.equals(Statics.SPOTA_SERVICE_UUID) && !scannedDevices.containsKey(device.getAddress())) {
                            scannedDevices.put(device.getAddress(), device);
                            if (bluetoothDeviceList.contains(device)) {
                                deviceNameList.remove(deviceNameList.get(bluetoothDeviceList.indexOf(device)));
                                bluetoothDeviceList.remove(device);
                                deviceNameList.add(new ScanItem(device.getName(), device.getAddress(), rssi, true));
                                bluetoothDeviceList.add(device);
                                bluetoothScanAdapter.notifyDataSetChanged();
                            } else {
                                bluetoothDeviceList.add(device);
                                Set<BluetoothDevice> bondedDevices = mBluetoothAdapter.getBondedDevices();
                                deviceNameList.add(new ScanItem(device.getName(), device.getAddress(), rssi, bondedDevices.contains(device)));
                                bluetoothScanAdapter.notifyDataSetChanged();
                            }
                        }
                    }
                }
            });
        }
    };

    private interface ScannerApi {
        void startScanning();

        void stopScanning();
    }

    @SuppressWarnings("deprecation")
    private ScannerApi scannerApi19 = new ScannerApi() {
        @Override
        public void startScanning() {
            mBluetoothAdapter.startLeScan(mLeScanCallback);
        }

        @Override
        public void stopScanning() {
            mBluetoothAdapter.stopLeScan(mLeScanCallback);
        }
    };

    private ScannerApi scannerApi21 = new ScannerApi() {
        BluetoothLeScanner scanner;
        ScanCallback callback;
        ScanSettings settings;

        @TargetApi(21)
        @Override
        public void startScanning() {
            if (scanner == null) {
                scanner = mBluetoothAdapter.getBluetoothLeScanner();
                settings = new ScanSettings.Builder().setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY).setReportDelay(0).build();
                callback = new ScanCallback() {
                    @Override
                    public void onScanResult(int callbackType, ScanResult result) {
                        mLeScanCallback.onLeScan(result.getDevice(), result.getRssi(), result.getScanRecord().getBytes());
                    }

                    @Override
                    public void onBatchScanResults(List<ScanResult> results) {
                        for (ScanResult result : results)
                            mLeScanCallback.onLeScan(result.getDevice(), result.getRssi(), result.getScanRecord().getBytes());
                    }
                };
            }
            scanner.startScan(null, settings, callback);
        }

        @TargetApi(21)
        @Override
        public void stopScanning() {
            if (scanner != null)
                scanner.stopScan(callback);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);
        scanTimer = new Runnable() {
            @Override
            public void run() {
                stopDeviceScan();
            }
        };
        deviceNameList = new ArrayList<>();
        bluetoothScanAdapter = new ScanAdapter(this,
                R.layout.scan_item_row, deviceNameList);
        String prevShowBondedDevices = Statics.getPreviousInput(this, R.id.show_bonded_devices);
        showBondedDevices = prevShowBondedDevices != null && Boolean.parseBoolean(prevShowBondedDevices);
        this.initialize();
        this.createFirmwareDirectory();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == REQUEST_ENABLE_BT) {
            // Make sure the request was successful
            if (resultCode == Activity.RESULT_OK) {
                this.startDeviceScan();
            }
        }
    }

    @Override
    protected void onDestroy() {
        stopDeviceScan();
        super.onDestroy();
    }

    private void initialize() {
        // Initialize layout variables
        setTitle(getResources().getString(R.string.app_devices_title));
        deviceListView = (ListView) findViewById(R.id.device_list);
        scannedDevices = new HashMap<String, BluetoothDevice>();
        bluetoothDeviceList = new ArrayList<BluetoothDevice>();

        // Initialize Bluetooth adapter
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null || !getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            // Device does not support Bluetooth Low Energy
            Log.e(TAG, "Bluetooth Low Energy not supported.");
            Toast.makeText(getApplicationContext(), "Bluetooth Low Energy is not supported on this device", Toast.LENGTH_LONG).show();
            finish();
        }

        scannerApi = Build.VERSION.SDK_INT < 21 ? scannerApi19 : scannerApi21;
        handler = new Handler();
        // If the bluetooth adapter is not enabled, request to enable it
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(
                    BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }
        // Otherwise, start scanning right away
        else {
            this.startDeviceScan();
        }
        deviceListView.setAdapter(bluetoothScanAdapter);
        deviceListView.setOnItemClickListener(this);

    }

    private void createFirmwareDirectory() {
        if (!storagePermission && !checkPermissions() || Statics.fileDirectoriesCreated(this))
            return;
        Log.d(TAG, "Create firmware directory");
        if (File.createFileDirectories(this))
            Statics.setFileDirectoriesCreated(this);
        else
            Log.e(TAG, "Firmware directory creation failed");
    }

    @TargetApi(23)
    private boolean checkPermissions() {
        if (permissionsOK)
            return true;
        if (permissionRequestPending)
            return false;

        // Check permissions
        locationPermission = checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
        storagePermission = checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
        permissionsOK = locationPermission && storagePermission;
        if (permissionsOK) {
            Log.d(TAG, "All permissions granted");
            return true;
        }

        // Request permissions
        permissionRequestPending = true;
        final String[] permissions = new String[]{
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
        };

        for (String perm : permissions) {
            if (shouldShowRequestPermissionRationale(perm)) {
                Log.d(TAG, "Showing permission rationale for " + perm);
                new AlertDialog.Builder(this)
                        .setTitle("Permission Request")
                        .setIcon(R.drawable.ic_info_outline_black_36dp)
                        .setMessage(R.string.permission_rationale)
                        .setCancelable(false)
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Log.d(TAG, "Requesting permissions");
                                requestPermissions(permissions, PERMISSION_REQUEST);
                            }
                        })
                        .show();
                return false;
            }
        }

        Log.d(TAG, "Requesting permissions");
        requestPermissions(permissions, PERMISSION_REQUEST);
        return false;
    }

    @TargetApi(23)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST && grantResults.length > 0) {
            permissionRequestPending = false;
            boolean granted = true;
            boolean shouldExit = false;
            for (int i = 0; i < permissions.length; ++i) {
                Log.d(TAG, "Permission " + permissions[i] + (grantResults[i] == PackageManager.PERMISSION_GRANTED ? " granted" : " denied"));
                granted = granted && grantResults[i] == PackageManager.PERMISSION_GRANTED;
                if (permissions[i].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    storagePermission = grantResults[i] == PackageManager.PERMISSION_GRANTED;
                    if (!shouldExit)
                        shouldExit = !storagePermission;
                } else if (permissions[i].equals(Manifest.permission.ACCESS_COARSE_LOCATION))
                    locationPermission = grantResults[i] == PackageManager.PERMISSION_GRANTED;
            }
            if (storagePermission) {
                createFirmwareDirectory();
            }
            if (granted) {
                Log.d(TAG, "All permissions granted");
                permissionsOK = true;
                startDeviceScan();
                return;
            }
            if (shouldExit) {
                Log.e(TAG, "Missing required permission");
                new AlertDialog.Builder(this)
                        .setTitle("Permission Denied")
                        .setIcon(R.drawable.ic_error_outline_black_36dp)
                        .setMessage(R.string.permission_denied)
                        .setPositiveButton(android.R.string.ok, null)
                        .setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                finish();
                            }
                        })
                        .show();
            }
        }
    }

    private void checkLocationServices() {
        if (Build.VERSION.SDK_INT < 23 || !scannedDevices.isEmpty())
            return;
        if (Settings.Secure.getInt(getContentResolver(), Settings.Secure.LOCATION_MODE, Settings.Secure.LOCATION_MODE_OFF) == Settings.Secure.LOCATION_MODE_OFF) {
            Log.d(TAG, "Location services disabled");
            if (displayedLocationServicesInfo)
                return;
            displayedLocationServicesInfo = true;
            new AlertDialog.Builder(this)
                    .setTitle("Location Services Disabled")
                    .setIcon(R.drawable.ic_info_outline_black_36dp)
                    .setMessage(R.string.no_location_services)
                    .setPositiveButton(R.string.enable_location_services, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                        }
                    })
                    .setNegativeButton(android.R.string.cancel, null)
                    .show();
        }
    }

    private void startDeviceScan() {
        if (!locationPermission && !checkPermissions()) {
            updateBondedDevices();
            return;
        }
        isScanning = true;
        bluetoothDeviceList.clear();
        scannedDevices.clear();
        deviceNameList.clear();
        bluetoothScanAdapter.clear();
        bluetoothScanAdapter.notifyDataSetChanged();
        if (menuItemRefresh != null) {
            menuItemRefresh.setTitle("STOP SCAN");
        }
        Log.d(TAG, "Start scanning");
        updateBondedDevices();
        scannerApi.startScanning();
        handler.postDelayed(scanTimer, 10000);
    }

    private void stopDeviceScan() {
        if (isScanning) {
            isScanning = false;
            handler.removeCallbacks(scanTimer);
            Log.d(TAG, "Stop scanning");
            setProgressBarIndeterminateVisibility(false);
            scannerApi.stopScanning();
            if (menuItemRefresh != null) {
                menuItemRefresh.setTitle("SCAN");
            }
            checkLocationServices();
        }
    }

    private void updateBondedDevices() {
        if (showBondedDevices) {
            for (BluetoothDevice device : mBluetoothAdapter.getBondedDevices()) {
                if (!bluetoothDeviceList.contains(device)) {
                    bluetoothDeviceList.add(device);
                    deviceNameList.add(new ScanItem(device.getName(), device.getAddress(), -900, true));
                }
            }
        } else {
            // Remove bonded devices that were not found during scan
            for (BluetoothDevice device : mBluetoothAdapter.getBondedDevices()) {
                if (!scannedDevices.containsKey(device.getAddress())) {
                    bluetoothDeviceList.remove(device);
                }
            }
            bluetoothScanAdapter.clear();
            for (BluetoothDevice device : bluetoothDeviceList) {
                deviceNameList.add(new ScanItem(device.getName(), device.getAddress(), -900, true));
            }
            bluetoothScanAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        boolean status = super.onCreateOptionsMenu(menu);
        menuItemRefresh = menu.findItem(R.id.restart_scan);
        menuItemRefresh.setVisible(true);

        MenuItem menuItemShowBonded = menu.findItem(R.id.show_bonded_devices);
        menuItemShowBonded.setVisible(true);
        menuItemShowBonded.setChecked(showBondedDevices);
        MenuItem menuItemAbout = menu.findItem(R.id.about);
        menuItemAbout.setVisible(true);
        return status;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.restart_scan:
                if (isScanning) {
                    stopDeviceScan();
                } else {
                    startDeviceScan();
                }
                break;

            case R.id.show_bonded_devices:
                showBondedDevices = !showBondedDevices;
                Statics.setPreviousInput(this, R.id.show_bonded_devices, String.valueOf(showBondedDevices));
                item.setChecked(showBondedDevices);
                //Rescan to show current RSSI
                if (isScanning) {
                    stopDeviceScan();
                    startDeviceScan();
                } else {
                    startDeviceScan();
                }
                break;

            case R.id.about:
                Intent intent = new Intent(ScanActivity.this, com.dialog.suota.activities.InfoActivity.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * On click listener for scanned devices
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {
        stopDeviceScan();
        BluetoothDevice device = bluetoothDeviceList.get(position);
        Intent i = new Intent(ScanActivity.this, DeviceActivity.class);
        i.putExtra("device", device);
        startActivity(i);
    }
}
