/*
 *******************************************************************************
 *
 * Copyright (C) 2016 Dialog Semiconductor, unpublished work. This computer
 * program includes Confidential, Proprietary Information and is a Trade
 * Secret of Dialog Semiconductor. All use, disclosure, and/or reproduction
 * is prohibited unless authorized in writing. All Rights Reserved.
 *
 * bluetooth.support@diasemi.com
 *
 *******************************************************************************
 */

package com.dialog.suota;

import android.app.Application;
import android.bluetooth.BluetoothDevice;

/**
 * Application wide values
 */
public class SuotaApplication extends Application {
    public BluetoothDevice device;
    public String infoManufacturer = "Unknown";
    public String infoModelNumber = "Unknown";
    public String infoFirmwareVersion = "Unknown";
    public String infoSoftwareRevision = "Unknown";

    public void resetToDefaults() {
        device = null;
        infoManufacturer = "Unknown";
        infoModelNumber = "Unknown";
        infoFirmwareVersion = "Unknown";
        infoSoftwareRevision = "Unknown";
    }
}
