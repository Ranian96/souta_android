/*
 *******************************************************************************
 *
 * Copyright (C) 2016 Dialog Semiconductor, unpublished work. This computer
 * program includes Confidential, Proprietary Information and is a Trade
 * Secret of Dialog Semiconductor. All use, disclosure, and/or reproduction
 * is prohibited unless authorized in writing. All Rights Reserved.
 *
 * bluetooth.support@diasemi.com
 *
 *******************************************************************************
 */

package com.dialog.suota.activities;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.WindowManager;

import com.dialog.suota.R;
import com.dialog.suota.fragments.SplashFragment;
import com.dialog.suota.global.BusProvider;
import com.dialog.suota.global.Utils;
import com.squareup.otto.Subscribe;

import java.util.Objects;

public class SplashActivity extends ActionBarActivity {

    // LIFE CYCLE METHOD(S)
    Fragment fragment;

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (Objects.equals(bundle.getString("f"), "info")) {
                fragment = new Fragment();
            } else {
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                        WindowManager.LayoutParams.FLAG_FULLSCREEN);
                fragment = new SplashFragment();
            }
        } else {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
            fragment = new SplashFragment();
        }
        setContentView(R.layout.activity_splash);
        Utils.replaceFragment(this, fragment, R.id.fragment_container, true);

    }

    @Override
    protected void onResume() {
        super.onResume();
        // Register ourselves so that we can provide the initial value.
        BusProvider.getInstance().register(this);
    }

    @Override
    protected void onPause() {
        // Always unregister when an object no longer should be on the bus.
        BusProvider.getInstance().unregister(this);
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @SuppressWarnings("unused")
    @Subscribe
    public void onSplashExit(final SplashFragment.SplashEvent event) {
        finish();
        Intent intent = new Intent(this, ScanActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        this.overridePendingTransition(0, 0);
    }
}