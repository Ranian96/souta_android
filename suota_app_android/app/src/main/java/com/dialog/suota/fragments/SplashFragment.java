/*
 *******************************************************************************
 *
 * Copyright (C) 2016 Dialog Semiconductor, unpublished work. This computer
 * program includes Confidential, Proprietary Information and is a Trade
 * Secret of Dialog Semiconductor. All use, disclosure, and/or reproduction
 * is prohibited unless authorized in writing. All Rights Reserved.
 *
 * bluetooth.support@diasemi.com
 *
 *******************************************************************************
 */

package com.dialog.suota.fragments;

import android.app.Fragment;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dialog.suota.R;
import com.dialog.suota.global.BusProvider;


public class SplashFragment extends Fragment {

    private static final int SPLASH_TIME = 3000;
    private static final String TAG = "SplashFragment";
    private Handler mHandler = new Handler();

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_splash, container, false);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BusProvider.getInstance().post(new SplashEvent());
            }
        });
        return view;
    }

    // LIFE CYCLE METHOD(S)

    @Override
    public void onResume() {
        super.onResume();
        View v = getView();
        TextView appName = null;
        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/MyriadPro-Light.otf");
        if (v != null) {
            appName = (TextView) v.findViewById(R.id.appName);
        } else {
            Log.e(TAG, "onCreateView: v is NULL!");
        }
        if (appName != null) {
            appName.setTypeface(typeface);
        } else {
            Log.e(TAG, "onCreateView: appName is NULL!");
        }
        // Register ourselves so that we can provide the initial value.
        BusProvider.getInstance().register(this);
        Runnable exitRunnable = new Runnable() {
            @Override
            public void run() {
                BusProvider.getInstance().post(new SplashEvent());
            }
        };
        this.mHandler.postDelayed(exitRunnable, SPLASH_TIME);
    }

    @Override
    public void onPause() {
        // Always unregister when an object no longer should be on the bus.
        BusProvider.getInstance().unregister(this);
        super.onPause();
    }

    @Override
    public void onDestroy() {
        mHandler.removeCallbacksAndMessages(null);
        super.onDestroy();
    }

    public class SplashEvent {
        //
    }
}