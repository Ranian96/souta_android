/*
 *******************************************************************************
 *
 * Copyright (C) 2016 Dialog Semiconductor, unpublished work. This computer
 * program includes Confidential, Proprietary Information and is a Trade
 * Secret of Dialog Semiconductor. All use, disclosure, and/or reproduction
 * is prohibited unless authorized in writing. All Rights Reserved.
 *
 * bluetooth.support@diasemi.com
 *
 *******************************************************************************
 */

package com.dialog.suota.global;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dialog.suota.R;

import java.util.ArrayList;

/**
 * ScanItem adapter for the device list on the main activity
 */
public class ScanAdapter extends ArrayAdapter<ScanItem> {

    Context mContext;
    int layoutResourceId;
    ArrayList<ScanItem> data = null;

    // @mContext - app context

    // @layoutResourceId - the scan_item_row.xml

    // @data - the ListItem data

    public ScanAdapter(Context mContext, int layoutResourceId, ArrayList data) {

        super(mContext, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            // inflate the scan_item_row.xml parent
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(layoutResourceId, parent, false);
        }

        // get the elements in the layout
        TextView textViewDialogTitle = (TextView) convertView.findViewById(R.id.textViewDialogTitle);
        TextView textViewDialogDescription = (TextView) convertView.findViewById(R.id.textViewDialogDescription);
        TextView textViewDialogSignal = (TextView) convertView.findViewById(R.id.textViewDialogSignal);
        SignalBar signalBar = (SignalBar) convertView.findViewById(R.id.signalBar);
        ImageView pairedImage = (ImageView) convertView.findViewById(R.id.pairedIcon);
        // Set the data for the list item. You can also set tags here if you want.
        ScanItem scanitem = data.get(position);

        if (!scanitem.scanPaired) {
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) textViewDialogTitle.getLayoutParams();
            params.addRule(RelativeLayout.ALIGN_PARENT_START);
            textViewDialogTitle.setLayoutParams(params);
            params = (RelativeLayout.LayoutParams) textViewDialogDescription.getLayoutParams();
            params.addRule(RelativeLayout.ALIGN_PARENT_START);
            textViewDialogDescription.setLayoutParams(params);
            pairedImage.setVisibility(View.GONE);
        } else {
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) textViewDialogTitle.getLayoutParams();
            params.removeRule(RelativeLayout.ALIGN_PARENT_START);
            textViewDialogTitle.setLayoutParams(params);
            params = (RelativeLayout.LayoutParams) textViewDialogDescription.getLayoutParams();
            params.removeRule(RelativeLayout.ALIGN_PARENT_START);
            textViewDialogDescription.setLayoutParams(params);
            pairedImage.setVisibility(View.VISIBLE);
        }

        textViewDialogTitle.setText(scanitem.scanName);
        textViewDialogDescription.setText(scanitem.scanDescription);
        textViewDialogSignal.setText(scanitem.scanSignal == -900 ? "" : String.valueOf(scanitem.scanSignal) + "dB");
        signalBar.setBool(scanitem.scanSignal >= -95, scanitem.scanSignal >= -85, scanitem.scanSignal >= -70, scanitem.scanSignal >= -60, scanitem.scanSignal == -900, scanitem.scanPaired);
        return convertView;
    }

}