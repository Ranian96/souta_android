/*
 *******************************************************************************
 *
 * Copyright (C) 2016 Dialog Semiconductor, unpublished work. This computer
 * program includes Confidential, Proprietary Information and is a Trade
 * Secret of Dialog Semiconductor. All use, disclosure, and/or reproduction
 * is prohibited unless authorized in writing. All Rights Reserved.
 *
 * bluetooth.support@diasemi.com
 *
 *******************************************************************************
 */

package com.dialog.suota.activities;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.dialog.suota.R;
import com.dialog.suota.SuotaApplication;
import com.dialog.suota.async.DeviceConnectTask;
import com.dialog.suota.bluetooth.BluetoothGattReceiver;
import com.dialog.suota.bluetooth.BluetoothGattSingleton;
import com.dialog.suota.bluetooth.BluetoothManager;
import com.dialog.suota.data.Statics;
import com.dialog.suota.fragments.DisclaimerFragment;
import com.dialog.suota.fragments.InfoFragment;
import com.dialog.suota.fragments.SUOTAFragment;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

public class DeviceActivity extends SuotaActivity {
    final static String TAG = "DeviceActivity";
    private BroadcastReceiver connectionStateReceiver, bluetoothGattReceiver, progressUpdateReceiver;
    private DeviceConnectTask connectTask;
    static DeviceActivity instance;
    SuotaApplication application;
    Toolbar toolbar;
    private Drawer drawer;
    public ProgressDialog dialog;
    private int previousFragmentID = 0;
    SecondaryDrawerItem disconnectButton;

    int menuSUOTA, menuDeviceInfo, menuDisclaimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        application = (SuotaApplication) getApplication();
        instance = this;
        setContentView(R.layout.activity_device);

        this.connectionStateReceiver = new BluetoothGattReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                super.onReceive(context, intent);
                int connectionState = intent.getIntExtra("state", 0);
                DeviceActivity.this.connectionStateChanged(connectionState);
            }
        };
        this.bluetoothGattReceiver = new BluetoothGattReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                super.onReceive(context, intent);
                if (previousFragmentID == 1) {
                    ((SUOTAFragment) getFragmentItem(1)).processStep(intent);
                }
                if (instance.dialog.isShowing()) {
                    instance.dialog.dismiss();
                }
            }
        };
        this.progressUpdateReceiver = new BluetoothGattReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                super.onReceive(context, intent);
                int progress = intent.getIntExtra("progess", 0);

                if (previousFragmentID == 1 && ((SUOTAFragment) getFragmentItem(1)).progressBar != null) {
                    ((SUOTAFragment) getFragmentItem(1)).progressBar.setProgress(progress);
                }
            }
        };


        toolbar = (Toolbar) findViewById(R.id.sensor_toolbar);
        if (toolbar != null) {
            toolbar.setTitleTextColor(Color.WHITE);
            setSupportActionBar(toolbar);
            toolbar.setTitle("SUOTA");
            toolbar.setSubtitle("");
        }
        LocalBroadcastManager.getInstance(this).registerReceiver(
                this.connectionStateReceiver,
                new IntentFilter(Statics.CONNECTION_STATE_UPDATE));
        LocalBroadcastManager.getInstance(this).registerReceiver(
                this.bluetoothGattReceiver,
                new IntentFilter(Statics.BLUETOOTH_GATT_UPDATE));
        LocalBroadcastManager.getInstance(this).registerReceiver(
                this.progressUpdateReceiver,
                new IntentFilter(Statics.PROGRESS_UPDATE));

        application.device = (BluetoothDevice) getIntent().getExtras().get("device");

        connectTask = new DeviceConnectTask(DeviceActivity.this, application.device) {
            @Override
            protected void onProgressUpdate(BluetoothGatt... gatt) {
                BluetoothGattSingleton.setGatt(gatt[0]);
            }
        };
        connectTask.execute();
        dialog = new ProgressDialog(this);
        dialog.setMessage("Connecting, please wait...");
        //dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                application.resetToDefaults();
                finish();
            }
        });
        dialog.show();

        IDrawerItem[] drawerItems = new IDrawerItem[4];
        int i = 0;
        drawerItems[i++] = new PrimaryDrawerItem().withName("Suota").withIcon(GoogleMaterial.Icon.gmd_system_update_alt);
        menuSUOTA = i;
        drawerItems[i++] = new DividerDrawerItem();
        drawerItems[i++] = new PrimaryDrawerItem().withName("Information").withIcon(R.drawable.cic_info).withSelectedIcon(R.drawable.cic_info_selected);
        menuDeviceInfo = i;
        drawerItems[i] = new PrimaryDrawerItem().withName("Disclaimer").withIcon(R.drawable.cic_disclaimer).withSelectedIcon(R.drawable.cic_disclaimer_selected);
        menuDisclaimer = i + 1;

        disconnectButton = new SecondaryDrawerItem() {
            @Override
            public void onPostBindView(IDrawerItem drawerItem, View view) {
                super.onPostBindView(drawerItem, view);
                view.setBackgroundColor(getResources().getColor(R.color.button_color));
            }

            @Override
            @LayoutRes
            public int getLayoutRes() {
                return R.layout.disconnect_drawer_button;
            }
        };

        disconnectButton
                .withTextColor(ContextCompat.getColor(this, android.R.color.white))
                .withName("Disconnect")
                .withIdentifier(300)
                .withEnabled(true);

        drawer = createNavDrawer(drawerItems);
        toolbar.setSubtitle("Device info");
        changeFragment(getFragmentItem(1), 1);
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy");
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
        BluetoothGattSingleton.getGatt().disconnect();
        BluetoothGattSingleton.getGatt().close();
        try {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(this.connectionStateReceiver);
            LocalBroadcastManager.getInstance(this).unregisterReceiver(this.bluetoothGattReceiver);
            LocalBroadcastManager.getInstance(this).unregisterReceiver(this.progressUpdateReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen()) {
            drawer.closeDrawer();
            return;
        }
        if (previousFragmentID == 1) {
            if (!((SUOTAFragment) getFragmentItem(1)).onBackPressed()) {
                super.onBackPressed();
            }
        } else {
            super.onBackPressed();
        }
    }

    private void connectionStateChanged(int connectionState) {
        if (connectionState == BluetoothProfile.STATE_DISCONNECTED) {
            Toast.makeText(DeviceActivity.this, application.device.getName() + " disconnected.", Toast.LENGTH_LONG).show();
            if (BluetoothGattSingleton.getGatt() != null) {
                // Refresh device cache if update was successful
                BluetoothManager.refresh(BluetoothGattSingleton.getGatt());
                BluetoothGattSingleton.getGatt().close();
            }
            application.resetToDefaults();
            finish();

        }
    }

    InfoFragment deviceInfoFragment;
    SUOTAFragment suotaFragment;
    DisclaimerFragment disclaimerFragment;

    public Fragment getFragmentItem(final int position) {
        if (position == menuDeviceInfo) {
            if (deviceInfoFragment == null) {
                deviceInfoFragment = new InfoFragment();
            }
            return deviceInfoFragment;
        } else if (position == menuSUOTA) {
            if (suotaFragment == null) {
                suotaFragment = new SUOTAFragment();
            }
            return suotaFragment;
        } else if (position == menuDisclaimer) {
            if (disclaimerFragment == null) {
                disclaimerFragment = new DisclaimerFragment();
            }
            return disclaimerFragment;
        } else {
            return new Fragment();
        }
    }

    private Drawer createNavDrawer(IDrawerItem[] drawerItems) {
        AccountHeader accountHeader = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.color.navigation_bar_background)
                .addProfiles(
                        new ProfileDrawerItem().withName("SUOTA").withEmail("Dialog Semiconductor")
                )
                .withProfileImagesClickable(false)
                .withProfileImagesVisible(false)
                .withSelectionListEnabledForSingleProfile(false)
                .withTextColor(getResources().getColor(android.R.color.white))
                .build();

        Drawer drawer = new DrawerBuilder().withActivity(this)
                .withAccountHeader(accountHeader)
                .withToolbar(toolbar)
                .addDrawerItems(drawerItems)
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        if (position == menuDeviceInfo) {
                            toolbar.setSubtitle("Information");
                        } else if (position == menuSUOTA) {
                            toolbar.setSubtitle("Suota");
                        } else if (position == menuDisclaimer) {
                            toolbar.setSubtitle("Disclaimer");
                        } else {
                            Log.d(TAG, "Pressed: " + String.valueOf(position));
                        }
                        changeFragment(getFragmentItem(position), position);
                        return false;
                    }
                })
                .addStickyDrawerItems(disconnectButton)
                .withStickyFooterShadow(false)
                .build();
        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(false);
            drawer.getActionBarDrawerToggle().setDrawerIndicatorEnabled(true);
        }
        return drawer;
    }

    public void changeFragment(Fragment newFragment, int position) {
        //Disconnect button
        if (position == -1) {
            finish();
        }

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, newFragment);
        fragmentTransaction.commit();

        previousFragmentID = position;
    }
}