/*
 *******************************************************************************
 *
 * Copyright (C) 2016 Dialog Semiconductor, unpublished work. This computer
 * program includes Confidential, Proprietary Information and is a Trade
 * Secret of Dialog Semiconductor. All use, disclosure, and/or reproduction
 * is prohibited unless authorized in writing. All Rights Reserved.
 *
 * bluetooth.support@diasemi.com
 *
 *******************************************************************************
 */

package com.dialog.suota.activities;

import android.content.res.AssetManager;
import android.os.Bundle;
import android.webkit.WebView;

import com.dialog.suota.R;
import com.dialog.suota.global.Utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class InfoActivity extends SuotaActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AssetManager assetManager = getAssets();
        setContentView(R.layout.activity_info);
        WebView webView = (WebView) findViewById(R.id.webView);

        try {
            BufferedReader r = new BufferedReader(new InputStreamReader(assetManager.open("info.html")));
            String html = Utils.readerToString(r);
            assert webView != null;
            webView.loadDataWithBaseURL("file:///android_asset/", html, "text/html", "utf-8", null);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

}
